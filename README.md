**SLOOP** is an OOP implementation in Lua which has a focus on simplicity, speed, and readability, but does not sacrifice the bare bones features.


The best way to explain SLOOP is through a demonstration of usage.


Classes are initialized by the `class(className, [superClass, [isSingleton]])` function which is returned by class.lua.


	local Enemy = class("Enemy")


Classes can be instantiated by `Enemy.new(...)`. The arguments should be provided for inside of the `Enemy:init()` method.


	function Enemy:init(name)
		self.Name = name
	end
	
	local myEnemy = Enemy.new("Phil")

Any methods established in classes applies to their instances and subclasses.
Methods in classes can be assigned after object instantiation and the objects will still know the methods.
This also applies to class-wide variables, unless they are set by the instance.

	function Enemy:walk(x, y)
		print("Walking to position (" .. x .. ", " .. y .. ")")
		-- Add some walking code here!
	end
	
	myEnemy:walk(2, 5)

> Walking to position (2, 5)

Metamethods can be established within the class (not the metatable of the class) and will function as metamethods.

	function Enemy:__tostring()
		return "My name is " .. self.Name
	end
	
	print(myEnemy)

> My name is Phil

Inheritance is done through the second argument of the class function.

	local Zombie = class("Zombie", Enemy)
	function Zombie:eat()
		print("Eating")
	end
	
	local myZombie = Zombie.new("Fred")
	myZombie:walk(-3, 4)
	myZombie:eat()

> Walking to position (-3, 4)
>
> Eating

- - - -

All classes have `Enemy.ClassName` and `Enemy.SuperClass`. The SuperClass is the class that the Enemy inherits. If not given a superclass to inherit, the class will inherit the ROOT class by default.

The ROOT class contains two methods: 

* `ROOT:__tostring()`: returns `ROOT.ClassName`

* `ROOT:isA(className)`: returns true if className matches the ClassName of that class.

* `ROOT:isChildOf(class)`: returns true if the SuperClass of that class is the given class.

* `ROOT:isDescendantOf(class)`: returns true if the class inherits the given class, otherwise false.

A more expanded example can be found in SLOOP/Vector3.lua
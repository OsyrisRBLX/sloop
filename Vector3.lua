local class = require("class.lua") -- Change this to an AssetId or Object Path for ROBLOX Usage!
-- Simple Vector3 Implementation. Could be easily expanded.
local Vector3 = class("Vector3")
function Vector3:init(x, y, z)
	self.x = x or 0
	self.y = y or 0
	self.z = z or 0
end
function Vector3:__tostring()
	return self.x .. ", " .. self.y .. ", " .. self.z
end
function Vector3:__add(other)
	if type(other) == "number" then
		return Vector3.new(self.x + other, self.y + other, self.z + other)
	else
		assert(other.IsA and other:IsA("Vector3"), "Value must be a Vector3 or number!")
		return Vector3.new(self.x + other.x, self.y + other.y, self.z + other.z)
	end
end
function Vector3:__sub(other)
	if type(other) == "number" then
		return Vector3.new(self.x - other, self.y - other, self.z - other)
	else
		assert(other.IsA and other:IsA("Vector3"), "Value must be a Vector3 or number!")
		return Vector3.new(self.x - other.x, self.y - other.y, self.z - other.z)
	end
end
function Vector3:__mul(other)
	if type(other) == "number" then
		return Vector3.new(self.x * other, self.y * other, self.z * other)
	else
		assert(other.IsA and other:IsA("Vector3"), "Value must be a Vector3 or number!")
		return Vector3.new(self.x * other.x, self.y * other.y, self.z * other.z)
	end
end
function Vector3:__div(other)
	if type(other) == "number" then
		return Vector3.new(self.x / other, self.y / other, self.z / other)
	else
		assert(other.IsA and other:IsA("Vector3"), "Value must be a Vector3 or number!")
		return Vector3.new(self.x / other.x, self.y / other.y, self.z / other.z)
	end
end
function Vector3:getMagnitude()
	return math.sqrt(self.x ^ 2 + self.y ^ 2 + self.z ^ 2)
end

print(Vector3.new(1, 2, 3) + Vector3.new(4, 5, 6))
--> 5, 7, 9
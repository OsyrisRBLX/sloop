ROOT = {}
ROOT.ClassName = "ROOT"
function ROOT:__tostring() return self.ClassName end
function ROOT:isA(className) return self.ClassName == className end
function ROOT:isChildOf(class)
	return rawequal(self.SuperClass, class)
end
function ROOT:isDescendantOf(class)
	repeat
		if self.SuperClass and rawequal(self.SuperClass, class) then
			return true
		end
		self = self.SuperClass
	until rawequal(class, ROOT) or not self
	return false
end
local mt = {
	__index = function(self, index) return (self.SuperClass and self.SuperClass[index]) or nil end,
	__call = function(self, ...) return self:__call(...) end,
	__concat = function(self, ...) return self:__concat(...) end,
	__unm = function(self) return self:__unm() end,
	__add = function(self, value) return self:__add(value) end,
	__sub = function(self, value) return self:__sub(value) end,
	__mul = function(self, value) return self:__mul(value) end,
	__div = function(self, value) return self:__div(value) end,
	__mod = function(self, value) return self:__mod(value) end,
	__pow = function(self, value) return self:__pow(value) end,
	__tostring = function(self) return self:__tostring() end,
	__metatable = {},
	__eq = function(self, value) return self:__eq(value) end,
	__lt = function(self, value) return self:__lt(value) end,
	__le = function(self, value) return self:__le(value) end,
}
return function(className, superClass, isSingleton)
	local self = {}
	self.ClassName = className
	self.SuperClass = superClass or ROOT
	self.new = not isSingleton and function(...)
		local instance = setmetatable({SuperClass = self}, mt)
		if self.init then self.init(instance, ...) end
		instance.new = function() error("Instances cannot be instantiated from!") end
		return instance
	end or nil
	setmetatable(self, mt)
	return self
end